
import React, { Component } from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import SurveyMonkey from 'react-native-survey-monkey'

export default class SurveyMonkeyExample extends Component {

    render() {
        return (
            <View style={styles.container}>
                <SurveyMonkey ref={r => this.surveyMonkeyRef = r} />
                <TouchableOpacity onPress={this.handleOnPress} style={styles.button}>
                    <Text>Take Survey!!</Text>
                </TouchableOpacity>
            </View>
        )
    }

    handleOnPress = () => {
        this.surveyMonkeyRef.showSurveyMonkey('N72YQG6')
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        padding: 10,
        paddingHorizontal: 20
    }
})
